import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class Result {
    public static void main(String[] args) throws IOException {

        FileReader data = new FileReader(args[0]);
        FileReader def = new FileReader(args[1]);
        ObjectMapper objectMapper = new ObjectMapper();

        List<Employee> employees = objectMapper.readValue(data, new TypeReference<List<Employee>>() {
        });

        ReportDefinition reportDefinition = objectMapper.readValue(def, ReportDefinition.class);

        List<Employee> output = new ArrayList<>();
        addEmployeeWithSalesPeriodLessThanPeriodLimit(employees, reportDefinition, output);

        output.sort(Comparator.comparing(Employee::getScore).reversed());

        FileWriter csvWriter = new FileWriter("result.csv");
        writeColumnsInCSV(csvWriter);

        writeEmployeesWithSameScoreInCSV(output, csvWriter, writeEmployeesWithinTopPerformersPercent(reportDefinition, output, csvWriter));

        System.out.println("result.csv is generating");
        csvWriter.close();
    }

    private static void addEmployeeWithSalesPeriodLessThanPeriodLimit(List<Employee> employees, ReportDefinition reportDefinition, List<Employee> output) {
        for (Employee employee : employees) {
            double score;
            if (reportDefinition.isUseExperienceMultiplier())
                score = employee.getTotalSales() / employee.getSalesPeriod() * employee.getExperienceMultiplier();
            else
                score = employee.getTotalSales() / employee.getSalesPeriod();

            employee.setScore(score);
            if (employee.getSalesPeriod() <= reportDefinition.getPeriodLimit()) {
                output.add(employee);
            }
        }
    }

    private static void writeColumnsInCSV(FileWriter csvWriter) throws IOException {
        csvWriter.append("Name");
        csvWriter.append(", ");
        csvWriter.append("Score");
        csvWriter.append("\n");
    }

    private static int writeEmployeesWithinTopPerformersPercent(ReportDefinition reportDefinition, List<Employee> output, FileWriter csvWriter) throws IOException {
        int limit = (int) Math.round(reportDefinition.getTopPerformersThreshold() * output.size() / 100), i;
        if (limit < 1)
            limit = 1;

        for (i = 0; i < output.size(); i++) {
            csvWriter.append(output.get(i).getName());
            csvWriter.append(", ");
            csvWriter.append(String.valueOf(output.get(i).getScore()));
            csvWriter.append("\n");

            limit--;
            if (limit == 0) {
                break;
            }
        }
        return i;
    }

    private static void writeEmployeesWithSameScoreInCSV(List<Employee> output, FileWriter csvWriter, int i) throws IOException {
        if (i < output.size() - 1) {
            while (output.get(i).getScore() == output.get(i + 1).getScore()) {
                csvWriter.append(output.get(i + 1).getName());
                csvWriter.append(", ");
                csvWriter.append(String.valueOf(output.get(i + 1).getScore()));
                csvWriter.append("\n");

                i++;
                if (i == output.size() - 1) {
                    break;
                }
            }
        }
    }
}
